# Modeling and Verification @ [University of Geneva](http://www.unige.ch)

This repository contains important information about this course.
**Do not forget to [watch](https://gitlab.unige.ch/Laurent.Bartholdi/smv2024) it** as it will allow us to send notifications for events, such as new exercises, homework, slides or fixes.

## Important Information and Links

* [Page on Gitlab: `https://gitlab.unige.ch/Laurent.Bartholdi/smv2024`](https://gitlab.unige.ch/Laurent.Bartholdi/smv2024)
* Courses are Monday 8:15 - 10:00
* Exercises are Monday 10:15 - 12:00
* You have free time on Friday 14:00 - 16:00 to work on the course/exercises/homeworks. During this period, the teacher and the assistant do not come. This is personal time for you.
* Team: Laurent Bartholdi (teacher), Aurélien Coet (assistant)
* Use Gitlab issues to communicate with us (public)
* For private matters send an email or come see us in our offices:
    * `laurent.bartholdi@gmail.com`
    * `aurelien.coet@unige.ch` (office 216)
* During this semester, you will have graded homework which will count for 50% of the final grade. The other 50% will be an exam.
* I include some very good lecture notes by Bernd Finkbeiner, `Automata, Games, and Verification`, on which the course will be loosely based.
* A more advanced text on the material is the book `Erich Grädel, Wolfgang Thomas, Thomas Wilke (Eds.) Automata, Logics, and Infinite Games: A Guide to Current Research. Lecture Notes in Computer Science 2500` It is included here for your convenience, but should not to be distributed.

## Rules

* You must do your homework in your private fork of the course repository.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, homework is personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.

## Homework
* All homeworks will be located in a `Homework` directory.
* Do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* Read the complete instructions **before** starting an assignment
* Follow the instructions given to you in the assignments

### Homework Deadlines
You have until 23:59:59 on these dates to **push** your solutions to *your* (private) repository.
Generally: No late submissions, no extensions, no exceptions, no dogs eating homework.
If you are in an unfortunate circumstance where you do need an extension, tell us beforehand.
(Sending an email two hours before the deadline is *not* beforehand).

| No.  |     1      |    2       |  
| ---- | ---------- | ---------- | 
| Date | 02.12.2024 | 16.12.2024 | 

### Tokens (Bonus)

For the whole semester, you have 2 tokens to give you extra times for your TPs.
One token equals an additional 24 hours to complete your TP.
A token is automatically consumed if you push your work after the deadline (even for a second) !

### Evaluation

 * Your final TP grade is the average of all of them.
 * Here is an example of the calculation.

   | TP1 | TP2 | TP3 | TP4 | TP5 | TP6 |
   | --- | --- | --- | --- | --- | --- |
   |  4  |  5  |  4  |  5  |  6  |  5  |

   Final TP grade = (4 + 5 + 4 + 5 + 6 + 5) / 6 = 4.85

### Getting Help

You should be old and smart enough to find the solutions to most problems by yourself. I.e.:  
*If you encounter any problems, solve them!*

In case there is a problem you cannot solve, here is the order of escalation:
  1. Google is your friend. (response time < 1 sec.)
  2. Read the manual. (response time < 1 min.)
  3. Ask a friend/colleague. (response time < 30 mins.)
  4. Stackoverflow. [Learn how](https://stackoverflow.com/help/how-to-ask) (response time < 12 hrs.)
  5. Course assistants. (response time < 2 days.)
  6. Professor. (response time ???)

### Homework 1

Instructions for homework 1 are available [here](./Homework/Homework 1/Instructions.pdf). 
The deadline for the homework is November the 25th 2024, 23:59. 
Your solutions for the exercises should be submitted in a pdf file, in the same folder as the instructions.
To submit your homework, push your solution on your fork and create a merge request on Gitlab from your fork to the course's main repository, on the branch with your name.

You are allowed to work in groups of 2. 
If you do, please mention clearly the names of the TWO members of the group in your solution file.

In part 7 of the homework, you need to use a tool called `Pecan`.
If you prefer to use Docker to run it rather than installing it on your machine, your classmate **Tanguy Cavagna** kindly provided a [Dockerfile](./Homework/Homework 1/Dockerfile) to do so.
You can use the following command to run it:

```bash
docker run --rm --mount type=bind,source=/host/path/to/homework/,target=/home/pecan/ReedOei/Pecan/exercises pecan-prover:latest python3 pecan.py exercises/my-nice-exercise.pn
```

You will need to change the `/host/path/to/homework/` path in the above to the actual path to the folder containing the Dockerfile on your machine, as well as `exercises/my-nice-exercise.pn` to the path to your own Pecan file.

### Homework 2

Instructions for homework 2 are available [here](./Homework/Homework 2/HW2.pdf). 
The deadline for the homework is December the 16th 2024, 23:59. 
Your solutions for the exercises should be submitted in the same folder as the instructions.
To submit your homework, push your solution on your fork and create a merge request on Gitlab from your fork to the course's main repository, on the branch with your name.

You are allowed to work in groups of 2. 
If you do, please mention clearly the names of the TWO members of the group in your solution file.

