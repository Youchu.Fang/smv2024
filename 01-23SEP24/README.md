# First class, 23 September 2024

A typical case use of verification

Crashes

A simple automata-based verification in concurrent processing

Convenient software tools: walnut, lean, proverX/vampire, clingo, spin, ...

The main goal: logics - automata - games

Finkbeiner Chapter 1.
