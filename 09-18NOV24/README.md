# Ninth class, 18 November 2024

Determinizing Büchi automata: not always possible, but can be done with different acceptance condition.

Müller automata: where there is a collection F = {F_1,...,F_n} of subsets of Q, and a run r is successful if Inf(r)=F_i for some i.

Theorem: Büchi-recognizable, Müller-recognizable and deterministic-Müller-recognizable are equivalent.

(Proof goes through semi-deterministic Büchi automata. We saw Müller ⇒ Büchi in class; Büchi⇒semi-deterministic Büchi is [Finkbeiner, Construction 6.5] while Semi-deterministic Büchi⇒deterministic Müller is [Finkbeiner, Construction 6.6].)

Other variants: parity automata (each state has an integer rank; a run r is accepting if the minimal rank in Inf(r) is even).

Even wilder: automata for which the runs themselves are not words in Q^ω but rather infinite trees with vertex labels in Q. Transitions can be disjunctive (as usual: one branch must be available) or conjunctive (all branches must be available). This increases the computational power by allowing parallel computations, but still defines the same class of languages.

Finkbeiner chapters 6 and 8.
